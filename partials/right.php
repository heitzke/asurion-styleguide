<article class="intro">
  <div class="container" id="<?php the_title(); ?>">
    <div class="content">
      <div class="col-sm-3 col-md-3">
        <?php 
          $html_field = get_field('pattern_image'); 
          if (!empty($html_field)) : ?>
          <img src="<?php the_field('pattern_image'); ?>" alt="Pattern Image" class="illustration left" />
        <?php endif; ?>
      </div>
      <div class="col-sm-8 col-sm-offset-1 col-md-8 col-md-offset-1">
      	<h1><?php the_title(); ?></a></h1>
      	<div class="entry">
      		<?php the_content(); ?>
      		<!-- a href="<?php the_permalink() ?>"><?php the_title(); ?></a-->
      	</div>
        <?php 
          $html_field = get_field('printed_example'); 
          if (!empty($html_field)) : ?>
            <div class="example">
              <h5 class="example-title">Example</h5>
              <div class="example-content">
                <?php the_field('printed_example'); ?>
              </div>
            </div>
        <?php endif; ?>
        <?php 
          $html_field = get_field('code_example'); 
          if (!empty($html_field)) : ?>
            <h5 class="prettyprint-title">HTML</h5>
            <pre class="prettyprint lang-html prettyprint-bottom"><?php the_field('code_example'); ?></pre>
        <?php endif; ?>
        <?php 
          $html_field = get_field('css_example'); 
          if (!empty($html_field)) : ?>
            <h5 class="prettyprint-title">CSS</h5>
            <pre class="prettyprint lang-css"><?php the_field('css_example'); ?></pre>
        <?php endif; ?>
       </div>
    </div>
  </div>
</article>
