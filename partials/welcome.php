<img src="<?php bloginfo('template_directory'); ?>/img/circles-bg.svg" alt="main circle image" class="main-circles">
<section class="welcome text-center">
  <div class="container">
    <h1><?php the_title(); ?></a></h1>
    <?php the_content(); ?>
  </div>
</section>
