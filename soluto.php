<?php /* Template Name: Soluto Pattern Template */ ?>

<?php get_header(); ?>
<div class="interior-soluto interior-welcome">
  <h1><?php the_title(); ?></h1>
</div>
<div class="container">
      <div class="content">
      <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
        <article class="post" id="post-<?php the_ID(); ?>">
          <div class="entry">
            <?php the_content(); ?>
            <?php wp_link_pages(array('before' => __('Pages: '), 'next_or_number' => 'number')); ?>
          </div>
          <?php edit_post_link(__('Edit this entry.'), '<p>', '</p>'); ?>
          <p><?php the_tags(); ?></p>
        </article>
      <?php endwhile; endif; ?>
    </div>
</div>
<?php get_footer(); ?>
