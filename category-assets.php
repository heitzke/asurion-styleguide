<?php get_header(); ?>
<div class="interior-welcome">
  <h1>Assets</h1>
</div>
<div class="container">
  <div class="content">
    <div class="col-sm-3">
      <ul class="sidebar">
        <?php
        $parentCatName = single_cat_title('',false);
        $parentCatID = get_cat_ID($parentCatName);
        $childCats = get_categories( 'child_of='.$parentCatID );
        if(is_array($childCats)):
          foreach($childCats as $child){ ?>
            <li class="main">
              <a href="#<?php echo $child->name; ?>" ><?php echo $child->name; ?></a>
              <ul>
            <?php query_posts('cat='.$child->term_id); while(have_posts()): the_post();?>
              <li <?php post_class() ?>>
                <a class="subcategory" href="#<?php $title = current(explode(' ', get_the_title())); echo $title; ?>"><?php the_title(); ?></a>
              </li>
            <?php
          endwhile;
          echo '</li></ul>';

          wp_reset_query();
          }
        endif;?>
      </ul>
    </div>
    <div class="col-sm-8 col-sm-offset-1">
      <?php
        $parentCatName = single_cat_title('',false);
        $parentCatID = get_cat_ID($parentCatName);
        $childCats = get_categories( 'child_of='.$parentCatID );
        if(is_array($childCats)):
        foreach($childCats as $child){ ?>
          <h1 class="main" id="<?php echo $child->name; ?>"><?php echo $child->name; ?></h1>
          <?php query_posts('cat='.$child->term_id);
          while(have_posts()): the_post(); $do_not_duplicate = $post->ID; ?>
            <article class="interior">
              <h2 id="<?php $title = current(explode(' ', get_the_title())); echo $title; ?>"><?php the_title(); ?></h2>
              <div class="entry">
                <?php the_content(); ?>
              </div>
              <?php 
                $html_field = get_field('printed_example'); 
                if (!empty($html_field)) : ?>
                  <div class="example">
                    <div class="example-content">
                      <?php the_field('printed_example'); ?>
                    </div>
                  </div>
              <?php endif; ?>
              <?php 
                $html_field = get_field('code_example'); 
                if (!empty($html_field)) : ?>
                  <div class="example html">
                    <pre class="prettyprint lang-html prettyprint-bottom"><?php the_field('code_example'); ?></pre>
                  </div>
              <?php endif; ?>
              <?php 
                $html_field = get_field('js_example'); 
                if (!empty($html_field)) : ?>
                  <div class="example js">
                    <pre class="prettyprint lang-js prettyprint-bottom"><?php the_field('js_example'); ?></pre>
                  </div>
              <?php endif; ?>
              <?php 
                $html_field = get_field('css_example'); 
                if (!empty($html_field)) : ?>
                  <div class="example css">
                    <pre class="prettyprint lang-css"><?php the_field('css_example'); ?></pre>
                  </div>
              <?php endif; ?>
             </article>
          <?php
          endwhile;
          wp_reset_query();
          }
          endif;?>
    </div>
  </div>
</div>
<?php get_footer(); ?>
