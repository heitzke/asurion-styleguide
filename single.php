<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
 <div class="interior-welcome">
  <h1><?php the_title(); ?></h1>
</div>
<div class="container">
  <div class="content">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article class="col-sm-10 col-sm-offset-1">
				<div class="entry-content">
					<?php the_content(); ?>			
          <?php 
            $html_field = get_field('printed_example'); 
            if (!empty($html_field)) : ?>
              <div class="example">
                <div class="example-content">
                  <?php the_field('printed_example'); ?>
                </div>
              </div>
          <?php endif; ?>
          <?php 
            $html_field = get_field('code_example'); 
            if (!empty($html_field)) : ?>
              <div class="example html">
                <pre class="prettyprint lang-html prettyprint-bottom"><?php the_field('code_example'); ?></pre>
              </div>
          <?php endif; ?>
          <?php 
            $html_field = get_field('js_example'); 
            if (!empty($html_field)) : ?>
              <div class="example js">
                <pre class="prettyprint lang-js prettyprint-bottom"><?php the_field('js_example'); ?></pre>
              </div>
          <?php endif; ?>
          <?php 
            $html_field = get_field('css_example'); 
            if (!empty($html_field)) : ?>
              <div class="example css">
                <pre class="prettyprint lang-css"><?php the_field('css_example'); ?></pre>
              </div>
          <?php endif; ?>
				</div>
				<?php edit_post_link(__('Edit this entry'),'','.'); ?>
			</article>
		<?php endwhile; endif; ?>
	</div>
</div>
<?php get_footer(); ?>
