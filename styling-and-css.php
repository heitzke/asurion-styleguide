<?php get_header(); ?>
  <p>This post is styling and css</p>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php 
    $html_field = get_field('layout_position'); 
      if ($html_field == 'left') : ?>
        <?php get_template_part( 'partials/left', 'index' ); ?>
      <?php elseif ($html_field == 'right') : ?>
        <?php get_template_part( 'partials/right', 'index' ); ?>
      <?php elseif ($html_field == 'welcome') : ?>
        <?php get_template_part( 'partials/welcome', 'index' ); ?>
    <?php endif; ?>
	<?php endwhile; ?>
	<?php else : ?>
		<h2><?php _e('Nothing Found','html5reset'); ?></h2>
	<?php endif; ?>
<?php get_footer(); ?>
