<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
?>
		<!--- Closing div for main-content ---->
		</main>
		<footer class="source-org vcard copyright">
		  <p class="text-center">
        &copy;<?php echo date("Y"); echo " "; bloginfo('name'); ?>
      </p>
		</footer>
	</div>
	<?php wp_footer(); ?>
  	<script src="<?php bloginfo('template_directory'); ?>/js/vendor/prettify.js"></script>
  	<script src="<?php bloginfo('template_directory'); ?>/js/vendor/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/vendor/asurion.js"></script>
    <script src="<?php bloginfo('template_directory'); ?>/js/vendor/modernizr.custom.js"></script>
  	<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>
  	<script src="<?php bloginfo('template_directory'); ?>/_/js/functions.js"></script>
  </body>
</html>
