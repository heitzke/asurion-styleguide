<?php
/**
 * @package WordPress
 * @subpackage HTML5-Reset-WordPress-Theme
 * @since HTML5 Reset 2.0
 */
 get_header(); ?>
   <div id="top">
    <h1>UX Library</h1>
    <hr class="divider" />
    <h3>Asurion UX Design Standards</h3>
  </div>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
    <?php 
    $html_field = get_field('home_content_position'); 
      if ($html_field == 'left') : ?>
        <?php get_template_part( 'partials/left', 'index' ); ?>
      <?php elseif ($html_field == 'right') : ?>
        <?php get_template_part( 'partials/right', 'index' ); ?>
      <?php elseif ($html_field == 'welcome') : ?>
        <?php get_template_part( 'partials/welcome', 'index' ); ?>
    <?php endif; ?>
	<?php endwhile; ?>
	<?php else : ?>
		<p>nothing to see here</p>
	<?php endif; ?>
<?php get_footer(); ?>
