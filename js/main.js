$(document).ready(function (){
  $('.carousel').carousel({
    interval: false
  });
  prettyPrint();
});
$('ul.sidebar  li a').on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({
     scrollTop: $(this.hash).offset().top - 125
    }, 500, function(){
  });
});

$('ul.sidebar').affix({
  offset: {
    top: 245
  }
});
$('body').scrollspy({ target: '.navigation' });

(function() {
  var triggerBttn = document.getElementById( 'trigger-overlay' ),
    overlay = document.querySelector( 'div.overlay' ),
    closeBttn = overlay.querySelector( 'a.overlay-close' ),
    transEndEventNames = {
      'WebkitTransition': 'webkitTransitionEnd',
      'MozTransition': 'transitionend',
      'OTransition': 'oTransitionEnd',
      'msTransition': 'MSTransitionEnd',
      'transition': 'transitionend'
    },
    transEndEventName = transEndEventNames[ Modernizr.prefixed( 'transition' ) ],
    support = { transitions : Modernizr.csstransitions };

  function toggleOverlay() {
    if( $(overlay).hasClass('open') ) {
      $(overlay).removeClass('open');
      $(overlay).addClass('overlay-close');
      var onEndTransitionFn = function( ev ) {
        if( support.transitions ) {
          if( ev.propertyName !== 'visibility' ) return;
          this.removeEventListener( transEndEventName, onEndTransitionFn );
        }
        $(overlay).removeClass('overlay-close');
        $('body').off('wheel.modal mousewheel.modal');
      };
      if( support.transitions ) {
        overlay.addEventListener( transEndEventName, onEndTransitionFn );
      }
      else {
        onEndTransitionFn();
      }
    }
    else if( !($(overlay).hasClass('overlay-close')) ) {
      $(overlay).addClass('open');
      $('body').on('wheel.modal mousewheel.modal', function () {return false;});
    }
  }
  triggerBttn.addEventListener( 'click', toggleOverlay );
  closeBttn.addEventListener( 'click', toggleOverlay );
})();

