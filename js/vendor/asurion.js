/**
* Checkbox Maker
*/

function cheekyBox() {
	document.createElement('cheekybox'); //crutch for IE
	$('cheekybox').each(function(){
		var cheekyid = $(this).attr('id');
		var nameid = 'name="' + cheekyid + '" id="' + cheekyid + '"';
		var divclass = $(this).attr('class');
		var size = ($(this).attr('size')=="large")? 'lg' : 'sm';
		var label = $(this).attr('label');
		var target = ($(this).attr('targetid') != undefined)? ' targetid="' + $(this).attr('targetid') + '"' : '';
		var checkboxinput = ($(this).attr('createinput')=="yes")? '<input type="hidden" ' + nameid + ' value="off" />' : '';
		
		var output = 
			'<div class="' + divclass + '">' + checkboxinput +
			'<div ' + nameid + ' class="cheekybox cheekybox-' + size + '" ' + target + ' ></div>' +
			'<label for="' + cheekyid + '" class="cheekybox-' + size + '-label">' + label + '</label>' +
			'</div>';
			
		$(this).replaceWith(output);
	});
	$('.cheekybox').click(function(){
		var cheekyid = $(this).attr('id');
		var targetid = $(this).attr('targetid');
		if($(this).hasClass('icon-checkmark')){
			$(this).removeClass('icon-checkmark');
			if(targetid != undefined){ $('#' + targetid + '.btn').removeClass('btn-primary').addClass('btn-disabled'); }
			$('input#' + cheekyid).val("off");
		}else{
			$(this).addClass('icon-checkmark');
			if(targetid != undefined){ $('#' + targetid + '.btn').removeClass('btn-disabled').addClass('btn-primary'); }
			$('input#' + cheekyid).val("on");
		}
	});		
}

/**
* dropDownSelect
*/

function dropDownSelect() {
  document.createElement('dropdownselect'); //crutch for IE
  $('dropdownselect').each(function(){
    var id = $(this).attr('id');
    var label = $(this).attr('label');
    var readonly = $(this).attr('readonly')   
    var inputclass = $(this).attr('inputclass');
    var inputname = $(this).attr('inputname');
    var defaultval = $(this).attr('defaultval');
    var placeholder = $(this).attr('placeholder');
    var iconclass = $(this).attr('iconclass');
    var listvals = $(this).attr('listvals');
    var listarray = listvals.split(',');
    
    var labeloutput = (label == "") ? '' : '<label for="' + id + '">' + label + '</label>\n';
    var labellistclass = (label == "") ? 'no-label' : 'has-label';

    var listoutput = '<ul class="ddslist ' + inputclass + '">';
    $.each(listarray, function() {
      var elements = this.split(':');
      var name = elements[0];
      var thumb = elements[1];
      
      var liclass = (thumb == null) ? 'class="no-thumb"' : 'class="has-thumb" style="background-image: url(' + thumb + ')"';
      
      listoutput += '<li input="' + inputname + '" ' + liclass + ' >' + name + '</li>';
    });
    listoutput += '</ul>';
    
    var output =
      labeloutput +
      '<div class="input-append dropdownselect ' + inputclass + '">\n' +
      '<input id="' + id + '" readonly="' + readonly + '"  placeholder="' + placeholder + '" class="' + inputclass + ' ddsinput" name="' + inputname + '" type="text" value="' + defaultval + '">\n' +
      '<span class="add-on"><i class="' + iconclass + '"></i></span>\n' +
      '</div>\n' +
      '<div class="dropdownselect-list ' + inputclass + ' ' + labellistclass + '">\n' +
      listoutput +
      '</div>\n';
          
    $(this).replaceWith(output);

    var inputheight = $('input#' + id).outerHeight();
    $('input#' + id).next('.add-on').css('height',inputheight-2);
    $('input#' + id).next('.add-on').children('i').css('line-height',inputheight-4 + 'px');
    
    if($('.ddslist li').hasClass('has-thumb')){
      $('.ddslist').addClass('has-thumb');
    }
  });
  
  $('.dropdownselect').click(function() {
    $(this).next('.err').remove();
    var inputwidth = $(this).find('.ddsinput').outerWidth();
    var inputname = $(this).find('.ddsinput').attr('name');
    $('li[input="' + inputname +'"].no-thumb').parent().css('width', inputwidth );
    $('li[input="' + inputname +'"].has-thumb').parent().css('width', inputwidth );
    $(this).next('.dropdownselect-list').show();
  });

  $('.dropdownselect-list li').click(function() {
    var selected = $(this).text();
    var targetinput = $(this).attr('input');
    $('input[name="' + targetinput + '"]').val(selected);
    $(this).parents('.dropdownselect-list').hide();
  });

  $(document).mouseup(function(e) {
      var container = $('.dropdownselect').next('.dropdownselect-list');
      if (container.has(e.target).length === 0){
          container.hide();
      }
  }); 
}
